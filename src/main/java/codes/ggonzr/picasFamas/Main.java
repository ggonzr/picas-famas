package codes.ggonzr.picasFamas;
import codes.ggonzr.picasFamas.machine.DroolsSession;
import codes.ggonzr.picasFamas.player.Player;

import java.util.concurrent.Semaphore;


/**
 * Permite iniciar el proceso de ejecucion y recuperar respuestas por parte del usuario
 */
public class Main {
    // Canal comun
    private Semaphore sem;

    // Jugador
    private Player player;

    // Maquina
    private DroolsSession machine;

    // Ejecucion principal
    public static void main(String[] args) {
        try {
            Main m = new Main();
            m.execute();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Ejecutar el proceso
    private void execute() throws InterruptedException {
        // Instanciar y ejecutar el programa
        this.sem = new Semaphore(0);
        this.player = new Player(sem);
        this.player.start();

        // Vamos a intentar dejar la intención de espera
        this.player.join();
    }
}
