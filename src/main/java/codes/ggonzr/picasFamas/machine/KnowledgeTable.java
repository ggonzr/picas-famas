package codes.ggonzr.picasFamas.machine;

import codes.ggonzr.picasFamas.dto.Answer;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Almacena todas las respuestas obtenidas por la retroalimentación del usuario a las preguntas
 * realizadas por la maquina. Asimismo, determina que digitos son aptos para incluir en las siguientes preguntas
 * y en que posición deberian colocarse.
 *
 * @version 1.0
 * @since April 27 2021
 */
public class KnowledgeTable {

    // Matriz con las preguntas realizadas al usuario
    // La dimension 1 (i) es el intento realizado
    // La dimension 2 (j) es el caracter preguntado en una determinada posicion en el intento (i)
    private char[][] historicoPreguntas;

    // Matriz con el número de picas y famas por cada intento (i)
    // La dimension 1 (i) es el intento realizado
    // La dimension 2 (j) es el número de picas o famas obtenido por el intento
    //    0 = Número de famas
    //    1 = Número de picas
    private int[][] picasFamas;

    // Matriz con las posiciones posibles que puede ocupar un digito
    private PosicionesNumero[] posicionesPosiblesDigito;

    // Número maximo de preguntas permitidas
    private int preguntasAdmitidas;

    // Preguntas realizadas actualmente
    private int preguntaActual;

    // Digitos de la respuesta
    private char[] digitosRespuesta;

    // Encontramos los digitos de la respuesta
    private boolean digRespuestaEncontrados;

    // Posicion rotada
    private int posicionRotada;

    // Numero de la jugada en la cual encontramos todos los digitos
    // TODO: Ojo de momento lo pienso solo con haber encontrado una fama, corregir y generalizar
    private int preguntaDigitosEncontrados;

    // He rotado anteriormente
    private boolean heRotado;

    // Constructor
    public KnowledgeTable(int pPreguntasAdmitidas) {
        preguntasAdmitidas = pPreguntasAdmitidas;
        picasFamas = new int[preguntasAdmitidas][2];
        historicoPreguntas = new char[preguntasAdmitidas][4];
        posicionesPosiblesDigito = getInitialPositions();
        preguntaActual = 0;
        digitosRespuesta = new char[4];
        digRespuestaEncontrados = false;
        preguntaDigitosEncontrados = -1;
        posicionRotada = -1;
        heRotado = false;
    }

    /**
     * Instancia las posiciones iniciales posibles que puede ocupar un digito
     *
     * @return PosicionesNumero[] Arreglo con las posiciones posibles que puede tener un digito
     */
    private PosicionesNumero[] getInitialPositions() {
        PosicionesNumero[] pn = new PosicionesNumero[10];
        for (int i = 0; i < 10; i++) {
            pn[i] = new PosicionesNumero(i);
        }
        return pn;
    }

    // Getters & Setters
    public int getPreguntasAdmitidas() {
        return preguntasAdmitidas;
    }

    public int getPreguntaActual() {
        return preguntaActual;
    }

    public int getPreguntaDigitosEncontrados() {
        return preguntaDigitosEncontrados;
    }

    public char[][] getHistoricoPreguntas() {
        return historicoPreguntas;
    }

    public boolean isDigRespuestaEncontrados() {
        return digRespuestaEncontrados;
    }

    public int[][] getPicasFamas() {
        return picasFamas;
    }

    public int getPosicionRotada() {
        return posicionRotada;
    }

    public void setPosicionRotada(int posicionRotada) {
        this.posicionRotada = posicionRotada;
    }

    /**
     * Incrementa el contador de las preguntas actuales.
     * Solo lo usaremos directamente para la pregunta inicial.
     */
    public void incrementPreguntaActual() {
        this.preguntaActual = this.preguntaActual + 1;
    }

    /**
     * Permite registrar una nueva pregunta en la tabla de conocimiento
     *
     * @param pAnswer Pregunta realizada al usuario
     */
    public void registerQuestion(Answer pAnswer) {
        this.historicoPreguntas[this.preguntaActual] = pAnswer.getPregunta();
    }

    /**
     * Permite registrar una respuesta obtenida por parte del usuario a la pregunta anterior realizada.
     *
     * @param pAnswer Respuesta obtenida por parte del usuario a la pregunta anterior realizada
     */
    public void registerAnswer(Answer pAnswer) {
        this.picasFamas[this.preguntaActual] = new int[]{pAnswer.getFamas(), pAnswer.getPicas()};
    }

    /**
     * Permite descartar un numero dado a que sus digitos no pertenecen a una posicion
     * de la respuesta
     *
     * @param pAnswer Respuesta obtenida por parte del jugador fisico
     */
    public void descartarNumero(Answer pAnswer) {
        for (char digito : pAnswer.getPregunta()) {
            int nd = Integer.parseInt("" + digito);
            this.posicionesPosiblesDigito[nd].digitoDescartado();
        }
    }

    public Answer aplicarRotacion(Answer pAnswer, int pPosicion) {
        int nCaracter = -1;
        char digitoAReemplazar = pAnswer.getPregunta()[pPosicion];
        int rDig = Integer.parseInt("" + digitoAReemplazar);
        List<PosicionesNumero> pn = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            int dig = i;
            PosicionesNumero pi = this.posicionesPosiblesDigito[dig];
            pn.add(pi);
        }
        // Ordenar la lista
        Collections.sort(pn);
        for (int i = 0; i < pn.size(); i++) {
            PosicionesNumero ac = pn.get(i);
            int pAc = ac.getPosicionesPosibles()[pPosicion];
            if (pAc == 1 &&
                ac.getDigito() != digitoAReemplazar &&
                !pAnswer.getPreguntaOriginal().contains("" + ac.getDigito())
            ) {
                nCaracter = ac.getDigito();
                System.out.println("[KnowledgeTable][aplicarRotacion()] Nuevo Digito: " + nCaracter);
                break;
            }
        }
        if (nCaracter == -1) {
            // Rotar el digito
            int rote = -1;
            if (this.heRotado) {
                if (pPosicion + 2 == 4) {
                    rote = 3;
                }
                else {
                    rote = pPosicion + 2;
                }
            }
            else {
                rote = pPosicion + 1;
            }
            char oldChar = pAnswer.getPregunta()[pPosicion];
            char dummy = "X".toCharArray()[0];
            char replace = pAnswer.getPregunta()[rote];
            System.out.println("[KnowledgeTable][aplicarRotacion()] Char replace: " + replace);
            String g = pAnswer.getPreguntaOriginal().replace(oldChar, dummy);
            String d = g.replace(replace, oldChar);
            String f = d.replace(dummy, replace);
            System.out.println("[KnowledgeTable][aplicarRotacion()] Nueva pregunta: " + f);
            this.heRotado = true;
            Answer rsp = new Answer(f);
            return rsp;
        }
        else {
            String newDigit = "" + nCaracter;
            char oldChar = pAnswer.getPregunta()[pPosicion];
            char replace = newDigit.toCharArray()[0];
            System.out.println("[KnowledgeTable][aplicarRotacion()] Char replace: " + replace);
            String newQuestion = pAnswer.getPreguntaOriginal().replace(oldChar, replace);
            System.out.println("[KnowledgeTable][aplicarRotacion()] Nueva pregunta: " + newQuestion);
            Answer rsp = new Answer(newQuestion);
            return rsp;
        }
    }

    /**
     * Vamos a ir rotando los digitos de la primera posicion si encontramos al menos una fama
     * @param pAnswer Respuesta dada por el usuario
     * @return Nueva posicion que debemos observar
     */
    public int rotacion(Answer pAnswer, int pPosicion) {
        int fpActual = this.getPreguntaActual() - 1;
        int famas = this.picasFamas[fpActual][0];
        int picas = this.picasFamas[fpActual][1];
        System.out.println("[KnowledgeTable][rotacion()] Posicion de analisis: " + pPosicion);
        System.out.println("[KnowledgeTable][rotacion()] Picas y Famas actuales: "+ picas + " " + famas);
        int nPosicion = pPosicion;

        if (famas < pAnswer.getFamas()) {
            // La posicion 1 (0) de la pregunta es valida
            this.confirmarDigitoPosicion(pAnswer.getPregunta()[pPosicion], pPosicion);
            nPosicion = nPosicion + 1;
        }
        else if (famas > pAnswer.getFamas()) {
            // La posicion 1 (0) de la respuesta anterior es valida
            this.confirmarDigitoPosicion(this.historicoPreguntas[fpActual][pPosicion], pPosicion);
            nPosicion = nPosicion + 1;
        }
        else {
            // Vamos a mirar si aumentaron las picas
            if (picas < pAnswer.getPicas()) {
                // Encontramos un nuevo digito útil en la primera posicion :D
                this.descartarDigitoCompletamente(this.historicoPreguntas[fpActual][pPosicion]);
                nPosicion = nPosicion + 1;
            }
            else if (picas > pAnswer.getPicas()) {
                this.descartarDigitoCompletamente(pAnswer.getPregunta()[pPosicion]);
                nPosicion = nPosicion + 1;
            }
            else {
                // Descartar ambos digitos en la posicion
                System.out.println("[KnowledgeTable][rotacion()] Descartando ambos digitos");
                System.out.println("[KnowledgeTable][rotacion()] Digito: " + this.historicoPreguntas[fpActual][pPosicion]);
                System.out.println("[KnowledgeTable][rotacion()] Digito: " + pAnswer.getPregunta()[pPosicion]);
                this.descartarPosicionDigito(this.historicoPreguntas[fpActual][pPosicion], pPosicion);
                this.descartarPosicionDigito(pAnswer.getPregunta()[pPosicion], pPosicion);
            }
        }
        return nPosicion;
    }

    /**
     * Permite determinar si la respuesta actual tiene 2 famas mas que la anterior
     * @return true si se cumple la condicion, false de lo contrario.
     */
    public boolean combinacion2Famas() {
        System.out.println("[KnowledgeTable][combinacion2Famas()] Combinacion 2 famas");
        for (int i = 0; i < this.getPreguntaActual() - 1; i++) {
            if(this.picasFamas[i][0] == 2) {
                System.out.println("[KnowledgeTable][combinacion2Famas()] Encontramos dos famas antes");
                return false;
            }
        }

        System.out.println("[KnowledgeTable][combinacion2Famas()] Confirmar las dos famas por primera vez");
        return true;
    }

    /**
     * Confirma los digitos de las 2 famas obtenidas
     * @param pAnswer Respuesta del usuario
     */
    public void confirmar2famas(Answer pAnswer) {
        char[] pA = this.historicoPreguntas[this.getPreguntaActual()];
        char[] aA = pAnswer.getPregunta();
        for (int i = 0; i < pA.length; i++) {
            if (pA[i] != aA[i]) {
                this.confirmarDigitoPosicion(aA[i], i);
                System.out.println("[KnowledgeTable][combinacion2Famas()] Confirmando posiciones");
            }
        }
    }

    public boolean anterioresPicas4() {
        int[] intentoMenos1 = this.picasFamas[this.getPreguntaActual() - 1];
        int[] intentoMenos2 = this.picasFamas[this.getPreguntaActual() - 2];
        int suma = intentoMenos1[0] + intentoMenos1[1] + intentoMenos2[0] + intentoMenos2[1];
        System.out.println("[KnowledgeTable][anterioresPicas4()] Sume: " + suma);
        return suma == 4;
    }

    public void descartarDigitoCompletamente(char digito) {
        int dig = Integer.parseInt("" + digito);
        this.posicionesPosiblesDigito[dig].digitoDescartado();
    }

    public void descartarDigitosNoVistos() {
        System.out.println("[KnowledgeTable][descartarDigitosNoVistos()] Descartando digitos");
        String all = new String(this.historicoPreguntas[this.getPreguntaActual() - 1]) +
                new String(this.historicoPreguntas[this.getPreguntaActual() - 2]);
        String dig = "1234567890";
        for (char c: dig.toCharArray()) {
            boolean flag = false;
            for (char d: all.toCharArray()) {
                if (c == d) {
                    flag = true;
                    break;
                }
            }
            if (!flag) {
                int numDig = Integer.parseInt("" + c);
                this.posicionesPosiblesDigito[numDig].digitoDescartado();
                System.out.println("[KnowledgeTable][descartarDigitosNoVistos()] Digito Descartado: " + numDig);
            }
        }
    }

    /**
     * Descarta los digitos de las posiciones de una pregunta, pues no obtenemos si quiera alguna fa
     * @param pAnswer Respuesta obtenida por parte del jugador fisico
     */
    public void descartarPreguntaPosiciones(Answer pAnswer) {
        char[] digPregunta = pAnswer.getPregunta();
        for (int i = 0; i < digPregunta.length; i++) {
            this.descartarPosicionDigito(digPregunta[i], i);
        }
    }

    /**
     * Permite indicar que ya encontramos todos los digitos de la respuesta y asi optimizar el calcul
     * OJO: Este metodo se ejecutará siempre de registrar la respuesta en la tabla. Por ende la posición de la
     * pregunta encontrada es la anterior.
     * @param pAnswer Respuesta dada por el usuario fisico.
     */
    public void digitosEncontrados(Answer pAnswer) {
        System.out.println("[KnowledgeTable][digitosEncontrados()] Reportando los digitos de la respuesta");
        this.digitosRespuesta = pAnswer.getPregunta();
        this.digRespuestaEncontrados = true;
        this.preguntaDigitosEncontrados = this.preguntaActual;

        // Descartar los demas digitos
        for (int i = 0; i < this.posicionesPosiblesDigito.length; i++) {
            if (!pAnswer.getPreguntaOriginal().contains("" + i)) {
                this.posicionesPosiblesDigito[i].digitoDescartado();
            }
        }
    }

    /**
     * Tenemos un problema por la modificacion y el disparo de reglas, espero que este mecanismo ayude
     * @param pAnswer Respuesta obtenida por el usuario fisico
     * @return true si la pregunta pasada por parametro es igual a la ultima registrada en la tabla
     */
    public boolean compareQuestions(Answer pAnswer) {
        String qActual = new String(this.historicoPreguntas[this.getPreguntaActual()]);
        return qActual.equals(pAnswer.getPreguntaOriginal());
    }

    /**
     * Confirma y descarta los digitos según la respuesta obtenida por el usuario y las rotaciones realizadas
     * @param pAnswer Respuesta dada por el usuario fisico.
     */
    public void confirmarYDescartarParejas(Answer pAnswer) {
        int famasAnterior = this.getPicasFamas()[this.getPreguntaActual()][0];
        int famasActual = pAnswer.getFamas();
        int rotacion = this.preguntaActual - this.preguntaDigitosEncontrados;

        if (rotacion == 1) {
            if (famasActual > famasAnterior) {
                this.confirmarDigitoPosicion(pAnswer.getPregunta()[0], 0);
                this.descartarPosicionDigito(pAnswer.getPregunta()[1], 1);
            }
            else if (famasActual < famasAnterior) {
                this.confirmarDigitoPosicion(pAnswer.getPregunta()[0], 1);
                this.descartarPosicionDigito(pAnswer.getPregunta()[1], 1);
            }
            else {
                this.descartarPosicionDigito(pAnswer.getPregunta()[0], 0);
                this.descartarPosicionDigito(pAnswer.getPregunta()[1], 1);
            }
        }
        else if (rotacion == 2) {
            if (famasActual > famasAnterior) {
                this.confirmarDigitoPosicion(pAnswer.getPregunta()[2], 2);
                this.descartarPosicionDigito(pAnswer.getPregunta()[3], 3);
            }
            else if (famasActual < famasAnterior) {
                this.confirmarDigitoPosicion(pAnswer.getPregunta()[3], 2);
                this.descartarPosicionDigito(pAnswer.getPregunta()[2], 3);
            }
            else {
                this.descartarPosicionDigito(pAnswer.getPregunta()[3], 3);
                this.descartarPosicionDigito(pAnswer.getPregunta()[2], 2);
            }
        }
    }

    public Answer darRotacion() {
        int rotacion = this.preguntaActual - this.preguntaDigitosEncontrados;
        Answer rsp = null;
        switch (rotacion) {
            case 0:
                rsp = darPreguntaRotacion1();
                break;
            case 1:
                rsp = darPreguntaRotacion2();
                break;
            case 2:
                rsp = darPreguntaRotacion3();
                break;
            default:
                rsp = darPreguntaDigitosEncontrados();
                break;
        }
        return rsp;
    }

    /**
     * Permite retornar una pregunta para el usuario fisico con base en las preguntas recibidas
     *
     * @return Una pregunta para el usuario fisico
     */
    public Answer darPregunta() {
        //return this.digRespuestaEncontrados ? darPreguntaDigitosEncontrados() : darPreguntaDigitosNoEncontrados();
        return null;
    }

    /**
     * Permite retornar una pregunta conociendo de antemano el espacio solucion de digitos posibles,
     * realizando la primera pregunta excluyente. Rotacion del digito 1 y 2
     * @return Una pregunta para el usuario fisico
     */
    private Answer darPreguntaRotacion1() {
        char[] digitos = getHistoricoPreguntas()[this.getPreguntaActual()];
        char temp = digitos[0];
        digitos[0] = digitos[1];
        digitos[1] = temp;
        Answer nAns = new Answer(new String(digitos));
        return nAns;
    }

    /**
     * Permite retornar una pregunta conociendo de antemano el espacio solucion de digitos posibles,
     * realizando la primera pregunta excluyente. Rotacion del digito 3 y 4
     * @return Una pregunta para el usuario fisico
     */
    private Answer darPreguntaRotacion2() {
        char[] digitos = getHistoricoPreguntas()[this.getPreguntaActual()];
        char temp = digitos[3];
        digitos[3] = digitos[2];
        digitos[2] = temp;
        Answer nAns = new Answer(new String(digitos));
        return nAns;
    }

    /**
     * Permite retornar una pregunta conociendo de antemano el espacio solucion de digitos posibles,
     * realizando la primera pregunta excluyente. Rotacion del digito 2 y 3
     * @return Una pregunta para el usuario fisico
     */
    private Answer darPreguntaRotacion3() {
        char[] digitos = getHistoricoPreguntas()[this.getPreguntaActual()];
        char temp = digitos[1];
        digitos[1] = digitos[2];
        digitos[2] = temp;
        Answer nAns = new Answer(new String(digitos));
        return nAns;
    }

    /**
     * Permite retornar una pregunta explorando los digitos posibles del espacio solucion sin conocerlos
     * concretamente aún
     *
     * @return Una pregunta para el usuario fisico
     */

    public Answer darPreguntaDigitosNoEncontrados() {
        int[] pQuestion = new int[]{0,0,0,0};
        char[] nQuestion = new char[4];
        List<PosicionesNumero> pn = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            int dig = i;
            PosicionesNumero pi = this.posicionesPosiblesDigito[dig];
            pn.add(pi);
        }
        // Ordenar la lista
        Collections.sort(pn);
        for (int i = 0; i < pn.size(); i++) {
            PosicionesNumero ac = pn.get(i);
            int[] pAc = ac.getPosicionesPosibles();
            for (int j = 0; j < pAc.length; j++) {
                if (pAc[j] == 1 && pQuestion[j] == 0) {
                    nQuestion[j] = Character.forDigit(ac.getDigito(), 10);
                    pQuestion[j] = 1;
                    break;
                }
            }
        }
        Answer rsp = new Answer(new String(nQuestion));
        return rsp;
    }

    public Answer darPreguntaDigitosEncontrados() {
        int[] pQuestion = new int[]{0,0,0,0};
        char[] nQuestion = new char[4];
        List<PosicionesNumero> pn = new ArrayList<>();
        for (int i = 0; i < this.digitosRespuesta.length; i++) {
            int dig = Integer.parseInt("" + this.digitosRespuesta[i]);
            PosicionesNumero pi = this.posicionesPosiblesDigito[dig];
            pn.add(pi);
        }
        // Ordenar la lista
        Collections.sort(pn);
        for (int i = 0; i < pn.size(); i++) {
            PosicionesNumero ac = pn.get(i);
            int[] pAc = ac.getPosicionesPosibles();
            for (int j = 0; j < pAc.length; j++) {
                if (pAc[j] == 1 && pQuestion[j] == 0) {
                    nQuestion[j] = Character.forDigit(ac.getDigito(), 10);
                    pQuestion[j] = 1;
                    break;
                }
            }
        }
        Answer rsp = new Answer(new String(nQuestion));
        return rsp;
    }

    /**
     * Permite confirmar que el digito se encuentra unicamente correcto en la posicion dada
     * @param digito: Digito a confirmar
     *                posicion: Posicion confirmar
     */
    public void descartarPosicionDigito(char digito, int posicion) {
        int nd = Integer.parseInt("" + digito);
        this.posicionesPosiblesDigito[nd].descartarPosicion(posicion);
    }

    public void confirmarDigitoPosicion(char digito, int posicion) {
        int nd = Integer.parseInt("" + digito);
        this.posicionesPosiblesDigito[nd].confirmarPosicion(posicion);
        System.out.println(String.format("Digito: %c, Posicion: %d confirmados", digito, posicion));
    }

    /**
     * Clonar la tabla de conocimiento con un nuevo Hash code
     * TODO: Esto es una rapida y puede generar fugas de memoria Pilas !
     * @return Tabla de conocimiento
     */
    public KnowledgeTable clone() {
        KnowledgeTable nKt = new KnowledgeTable(this.preguntasAdmitidas);
        nKt.picasFamas = this.picasFamas;
        nKt.historicoPreguntas = this.historicoPreguntas;
        nKt.posicionesPosiblesDigito = this.posicionesPosiblesDigito;
        nKt.preguntaActual = this.preguntaActual;
        return nKt;
    }

    /**
    @Override
    public String toString() {
        return "KnowledgeTable{" +
                "historicoPreguntas=" + printMatrix(getHistoricoPreguntas()) +
                '}';
    }

    private String printMatrix(char[][] q) {
        String a = "";
        for (int i = 0; i < getPreguntaActual(); i++) {
            char[] qs = q[i];
            a += String.format("Posicion %d - Pregunta %s\t", i, new String(qs));
        }
        System.out.println(a);
        return a;
    }
    */

    /**
     * Una clase pequeña para representar cuales son las posibles posiciones que puede
     * ocupar un digito y el número agregado de posiciones posibles que se pueden usar.
     */
    private class PosicionesNumero implements Comparable<PosicionesNumero>{
        // Digito
        private int digito;

        // Numero de posiciones posibles (i) donde i \in {0, ... ,3}
        // El número 1 simboliza que la posicion está disponible para ser preguntada
        // El número 0 que ya hemos validado que el digito no se encuentra posible en esa posicion
        private int[] posicionesPosibles;

        // Numero total de posiciones posibles que aun se pueden usar
        private int agregadoPosiciones;

        /**
         * Permite crear un nuevo digito
         *
         * @param pDigito digito sobre el cual se van a representar las posibilidades
         */

        public PosicionesNumero(int pDigito) {
            this.digito = pDigito;
            this.posicionesPosibles = new int[]{1, 1, 1, 1};
            this.agregadoPosiciones = 4;
        }

        /**
         * Nos permite descartar completamente que el digito se encuentre presente
         * en alguna posicion de la respuesta
         */
        public void digitoDescartado() {
            this.agregadoPosiciones = 0;
            this.posicionesPosibles = new int[]{0, 0, 0, 0};
        }

        /**
         * Nos permite descartar que el digito se encuentre en la posicion p
         * @param pPosicion Posicion que vamos a descargar pPosicion \in {0, .., 3}
         */
        public void descartarPosicion(int pPosicion) {
            this.posicionesPosibles[pPosicion] = 0;
            this.agregadoPosiciones--;
        }

        /**
         * Nos permite confirmar que el digito realmente pertenece a esa posicion
         * @param pPosicion Posicion sobre la que vamos a confirmar el digito
         */
        public void confirmarPosicion(int pPosicion) {
            for (int i = 0; i < this.posicionesPosibles.length; i++) {
                if (i != pPosicion) {
                    this.posicionesPosibles[i] = 0;
                    this.agregadoPosiciones--;
                }
            }
        }

        // Getters
        public int getDigito() {
            return digito;
        }

        public int[] getPosicionesPosibles() {
            return posicionesPosibles;
        }

        public int getAgregadoPosiciones() {
            return agregadoPosiciones;
        }

        @Override
        public int compareTo(PosicionesNumero o) {
            return this.getAgregadoPosiciones() - o.getAgregadoPosiciones();
        }
    }
}