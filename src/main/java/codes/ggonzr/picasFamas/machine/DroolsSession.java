package codes.ggonzr.picasFamas.machine;

import codes.ggonzr.picasFamas.dto.Answer;
import codes.ggonzr.picasFamas.player.Player;
import org.kie.api.KieServices;
import org.kie.api.event.rule.DebugAgendaEventListener;
import org.kie.api.event.rule.DebugRuleRuntimeEventListener;
import org.kie.api.logger.KieRuntimeLogger;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

/**
 * Instancia el motor para realizar la ejecucion de las reglas (KIE Server)
 * Asimismo, permite crear todos los turnos para ser preguntados al usuario y obtener sus respectivas
 * respuestas.
 *
 * @version 1.0
 * @since April 27 2021
 */
public class DroolsSession {

    // Nueva sesion del motor de reglas
    private static KieSession ksession = null;

    // Logger de la sesion
    private static KieRuntimeLogger logger = null;

    // Tabla de conocimiento
    private static KnowledgeTable kt;

    // Referencia al jugador
    private Player playerRef;

    // Fin del juego
    private boolean finished;

    // Constructor
    public DroolsSession(int pPreguntasAdmitidas, Player pRef)
    {
        // Instanciar la tabla de conocimiento (registro)
        kt = new KnowledgeTable(pPreguntasAdmitidas);

        // El juego acaba de comenzar
        this.finished = false;

        // Referencia al jugador
        this.playerRef = pRef;
    }

    /**
     * Instancia el servicio del motor de reglas
     * @throws RuntimeException en caso de existir un error instanciando el motor de reglas
     */
    public void instantiate() {
        try {
            if (ksession == null || logger == null) {
                // From the kie services, a container is created from the classpath
                KieServices ks = KieServices.get();
                KieContainer kc = ks.getKieClasspathContainer();

                // Instanciar el motor de reglas
                execute(ks, kc);
            }
        } catch (Exception e) {
            System.out.println("[DroolsSession] Error fatal iniciando el motor de reglas");
            e.printStackTrace();
            e.getCause().printStackTrace();
        }
    }

    /**
     * Permite inicializar el motor de reglas configurando la sesión, el logger y el buffer para recibir
     * cada una de las preguntas a validar.
     *
     * @param ks KieServices Servicio para soportar la ejecución del motor de reglas
     * @param kc KieContiner Contenedor para el entorno de la sesión a ejecutar
     */
    private void execute(KieServices ks, KieContainer kc) {
        // From the container, a session is created based on
        // its definition and configuration in the META-INF/kmodule.xml file
        ksession = kc.newKieSession("PicasFamasKS");

        // Variables globales para el motor de reglas
        // =============================================

        // Y a pasar como referencia la sesión que estamos ejecutando
        ksession.setGlobal("dSession", this);

        // The application can also setup listeners
        ksession.addEventListener(new DebugAgendaEventListener());
        ksession.addEventListener(new DebugRuleRuntimeEventListener());

        // Set up a file based audit logger
        logger = ks.getLoggers().newFileLogger(ksession, "./logs/helloworld");

        // To set up a ThreadedFileLogger, so that the audit view reflects events whilst debugging,
        // uncomment the next line
        // KieRuntimeLogger logger = ks.getLoggers().newThreadedFileLogger( ksession, "./helloworld", 1000 );

        // Insertar la tabla de conocimiento
        ksession.insert(kt);

        // and fire the rules
        ksession.fireAllRules();
    }

    /**
     * Permite cerrar correctamente todas las instancias: Sesión en KIE y el logger
     * asi como el canal de comunicacion: Channel<Answer>
     * @throws RuntimeException en caso de existir un error cerrando el servicio del motor de reglas o logger
     */
    private synchronized void disposeSession() {
        try {
            // Close logger
            logger.close();

            // and then dispose the session
            ksession.dispose();

            // Cerrar la conexion
            this.finished = true;
        } catch (Exception e) {
            System.out.println("[DroolsSession] Error fatal cerrando la sesión del motor de reglas o el logger");
            e.printStackTrace();
        }
    }

    /**
     * Determina si el juego ya finalizo
     * @return true si el juego finalizo, false de lo contrario
     */
    public synchronized boolean isFinished() {
        return this.finished;
    }

    /**
     * Permite registrar una nueva respuesta brindada por el usuario
     *
     * @param pAnswer Pregunta realizada por la maquina junto con la respuesta dada por el usuario
     *                - número de picas y famas
     */
    public void addAnswer(Answer pAnswer) {
        try {
            System.out.println("[Machine][addAnswer()] Recibiendo respuesta");

            // Insertar de nuevo la tabla de conocimiento
            //ksession.insert(kt.clone());

            // The application can insert facts into the session
            ksession.insert(pAnswer);

            // and fire the rules
            ksession.fireAllRules();

            System.out.println("[Machine][addAnswer()] Respuesta agregada al motor de reglas");
        } catch (Exception e) {
            disposeSession();
            e.printStackTrace();
        }
    }

    /**
     * Permite finalizar el juego
     */
    public void finishGame() {
        System.out.println("======== Maquina ========");
        System.out.println("Parece que he ganado e___e :D");
        System.out.println("Muchas gracias por jugar conmigo");
        System.out.println("=========================");
        System.out.println("");

        // Cerrar el juego
        this.disposeSession();
    }

    public void sendQuestionToPlayer (Answer pAnswer) {
        // Enviar la jugada
        this.playerRef.addQuestion(pAnswer);
    }
}
