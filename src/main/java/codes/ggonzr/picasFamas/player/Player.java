package codes.ggonzr.picasFamas.player;
import codes.ggonzr.picasFamas.Main;
import codes.ggonzr.picasFamas.dto.Answer;
import codes.ggonzr.picasFamas.machine.DroolsSession;

import java.nio.channels.ClosedChannelException;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.Semaphore;

public class Player extends Thread {
    // Referencia al buffer
    private Semaphore smp;

    // Referencia a Main
    //private Main mainReference;

    // Scanner para tomar los datos de entrada
    private Scanner sc;

    // Lista de preguntas
    private LinkedList<Answer> ans;

    // Drools Session
    private DroolsSession droolsRef;

    public Player(Semaphore pSemaphore) {
        this.smp = pSemaphore;
        this.sc = new Scanner(System.in);
        this.ans = new LinkedList<>();
    }

    @Override
    public void run() {
        // Dar las instrucciones del juego
        darInstrucciones();

        // Preguntar por el numero maximo de preguntas
        int maximoJugadas = maximoJugadas();

        // Instanciar la sesion de juego
        this.droolsRef = new DroolsSession(maximoJugadas, this);

        // Iniciar el juego
        if (this.instantiateSession(maximoJugadas)) {
            // Arrancar el juego
            try {
                jugar();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        else {
            System.out.println("[Player] Error fatal iniciando la sesión de juego");
        }
    }

    /**
     * Permite capturar todas las jugadas por parte del jugador fisico
     */
    private void jugar() throws InterruptedException {
        // Mientras no haya finalizado el juego.
        // Vamos a realizar la espera con mecanismos de espera pasiva.
        // Todos quedan embebidos en el canal.
        while (!this.droolsRef.isFinished()) {
            this.smp.acquire();
            // Obtener la jugada propuesta por la maquina
            Answer jMaquina = this.ans.removeFirst();
            // Mostrar los datos de la jugada
            System.out.println(String.format("La maquina pregunta: %s", jMaquina.getPreguntaOriginal()));
            // Preguntar por el numero de picas y famas
            System.out.println("A continuacion indique el número de picas obtenido por la pregunta");
            int picas = this.sc.nextInt();
            System.out.println("A continuacion indique el número de famas obtenido por la pregunta");
            int famas = this.sc.nextInt();
            // Enviar la respuesta a la maquina.
            jMaquina.setPicas(picas);
            jMaquina.setFamas(famas);
            this.droolsRef.addAnswer(jMaquina);
            System.out.println("===== Mensaje enviado a la maquina con exito");
            System.out.println();
        }
    }

    /**
     * Añade una pregunta para el jugador
     * @param pAnswer Pregunta a realizar
     */
    public synchronized void addQuestion(Answer pAnswer) {
        System.out.println("[Player][addQuestion()] Recibiendo pregunta por parte de la maquina");
        // Añadir pregunta
        this.ans.push(pAnswer);
        System.out.println("[Player][addQuestion()] Liberando el siguiente turno");
        // Realizar siguiente turno
        this.smp.release();
        System.out.println("[Player][addQuestion()] Turno concedido");
    }

    /**
     * Pregunta por el número maximo de jugadas que se pueden realizar
     * @return Número maximo de jugadas posibles
     */
    private int maximoJugadas() {
        System.out.println("Por favor ingrese el número maximo de jugadas posibles");
        int jugadas = this.sc.nextInt();
        System.out.println(String.format("Número maximo de jugadas ingresado: %d", jugadas));
        return jugadas;
    }

    /**
     * Imprime las instrucciones del juego
     */
    private void darInstrucciones() {
        List<String> msg = new LinkedList<String>();
        String ph = "==============================================";
        msg.add(ph);
        msg.add("Bienvenido al juego de Picas y Famas");
        msg.add("Piensa en un numero de 4 digitos NO repetidos y yo intentare descifrarlo");
        msg.add("Para lograrlo, voy a necesitar un poco de tu ayuda");
        msg.add("Por cada pregunta que yo te realice voy a solicitarte el número de Picas y Famas");
        msg.add(
                "Por Picas entendamos el número de digitos preguntados que pertenecen a la respuesta " +
                        "pero que se encuentran en la posicion incorrecta");
        msg.add(
                "Por Famas entendamos el número de digitos preguntados que pertenecen a la respuesta " +
                        "y se encuentran en la posicion correcta");
        msg.add("");
        msg.add("Con ello en mente, preparemonos para comenzar");
        msg.add(ph);
        msg.add("");

        // Imprimir los mensajes
        for (String m: msg) {
            System.out.println(m);
        }
    }

    /**
     * Instancia la sesión de juego con el número maximo de movimientos
     * @param pMaxMovements Número maximo de movimientos
     * @return true si el proceso de creación de la sesión de juego es satisfactorio
     *         false de lo contrario
     */
    private boolean instantiateSession(int pMaxMovements) {
        // Instanciar la sesion
        this.droolsRef.instantiate();

        System.out.println("====================");
        System.out.println("Bienvenido, soy tu oponente, un sistema basado en reglas");
        System.out.println("Te voy a ganar en esta partida e___e");
        System.out.println("====================");
        System.out.println("");
        return true;
    }
}
