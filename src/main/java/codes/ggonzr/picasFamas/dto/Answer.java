package codes.ggonzr.picasFamas.dto;

/**
 * Objeto DTO - POJO para representar la respuesta obtenida al realizar una pregunta
 * al usuario.
 * @since April 27 2021
 * @version 1.0
 */
public class Answer {

    // Pregunta original
    private String preguntaOriginal;

    // Caracteres de la pregunta realizada al usuario
    private char[] pregunta;

    // Numero de picas obtenidas (caracteres pertenecientes a la respuesta ...
    // pero en una posicion incorrecta)
    private int picas;

    // Numero de famas obtenidas (caracteres pertenecientes a la respuesta ...
    // en la posicion correcta)
    private int famas;

    public Answer(String pPregunta) {
        this.preguntaOriginal = pPregunta;
        this.pregunta = pPregunta.toCharArray();
        this.picas = 0;
        this.famas = 0;
    }

    // Getters & Setters

    public String getPreguntaOriginal() {
        return preguntaOriginal;
    }

    public char[] getPregunta() {
        return pregunta;
    }

    public int getPicas() {
        return picas;
    }

    public void setPicas(int picas) {
        this.picas = picas;
    }

    public int getFamas() {
        return famas;
    }

    public void setFamas(int famas) {
        this.famas = famas;
    }

    @Override
    public Answer clone() {
        Answer clone = new Answer(this.preguntaOriginal);
        clone.setFamas(this.famas);
        clone.setPicas(this.picas);
        return clone;
    }
}
